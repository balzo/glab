<?php

namespace App\Http\Controllers;

use App\Http\Requests\InscriptionRequest;
use App\Models\Villes;
use Illuminate\Http\Request;

class InscriptionController extends Controller
{
    public function index()
    {
        $villes = Villes::orderBy('name','ASC')->get();
        return view('forms.form',compact('villes'));
    }

   
}
