<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Villes;

class ApprenantController extends Controller
{
    public function index()
    {
        $villes = Villes::orderBy('name','ASC')->get();
        return view('forms.form',compact('villes'));
    }

    public function store(Request $request)
    {
       $request->validate([
            "nom"=>"required",
            "prenom"=>"required",
            "matricule"=>"required|max:3"
        ]);

        dd($request->all());
    }
}
