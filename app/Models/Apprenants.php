<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apprenants extends Model
{
    use HasFactory;

    protected $table = "apprenants";

    protected $fillable = [
        "nom","prenom","matricule","ville_id","contact"
    ];

}
